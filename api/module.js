var Api = function(path, express, bodyParser, passport, FacebookStrategy, store, utils) {
	var instance;

	function init() {

		app = express();

		app.use(bodyParser());
		app.set("port", 3000);

		app.use(passport.initialize());
		app.use(passport.session());

		app.use(express.static(path.join(__dirname, '../public')));
		app.use('/bower_components', express.static(path.join(__dirname, '../bower_components')));


		passport.serializeUser(function(user, done) {
			done(null, user);
		});

		passport.deserializeUser(function(user, done) {
			done(null, user);
		});

		// -- Facebook strategy
		passport.use('facebook', new FacebookStrategy({
				//localhost
				// clientID: '1094858587232116',
				// clientSecret: 'd8aa4c663056d636c40eea72d03326c4',
				// callbackURL: 'http://localhost:3000/auth/facebook/callback',

				//prod
				clientID: '188883404791476',
				clientSecret: 'dde21b50a31417a6e5915ac126a32472',
				callbackURL: 'http://ip.mobilike.rocks/auth/facebook/callback',
				profileFields: ['id', 'displayName', 'photos', 'email', 'gender', 'name', 'profileUrl']
			},
			function(req, accessToken, refreshToken, profile, done) {

				return done(null, profile);

			}
		));


		//endpoint for checking project status
		app.use('/health', function(req, res) {
			var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] || req.connection.remoteAddress;
			var query = JSON.stringify(req.query);
			res.send({
				code: 200,
				message: 'SUCCESS',
				ip: ip,
				query: req.query.mobilike
			});
		});

		//check for user IP value and return if any user from the same IP enrolled in campaign
		app.get('/check', function(req, res) {

			var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] || req.connection.remoteAddress;

			var query = {
				ip: ip
			};

			store.count('users', query).then(function(total) {
				//Case if no one has campaigned from this specific IP
				if (total == 0) {
					res.send({
						code: 200,
						message: 'SUCCESS',
						ip: ip
					});
				} else {


					if (req.query.cookie) {
						query = {
							ip: ip,
							cookie: {
								$ne: req.query.cookie
							}
						};
						total = total - 1;
					};

					var random = Math.floor(Math.random() * total);
					var options = {
						'skip': random,
						'limit': 1
					};

					console.log(query);
					return store.find('users', query, options);
				}
			}).then(function(data) {
				//If data is not empty, there are users from the same IP different than current user. If empty, the user is only registered user from this IP.

				res.send({
					code: 200,
					message: 'SUCCESS',
					ip: ip,
					data: data,
					query: query
				});
			}).fail(function(err) {
				res.send({
					code: 500,
					message: 'FAIL_SYSTEM',
					ip: ip,
					data: err
				});
			});
		});

		app.post('/save', function(req, res) {

			var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] || req.connection.remoteAddress;


			var user = {
				name: req.body.name,
				email: req.body.email,
				ip: ip,
				selection: req.body.selection,
				cookie: req.body.cookie
			};

			store.save('users', user).then(function(user) {
				res.send({
					code: 200,
					message: "SUCCESS",
					user: user
				});
			}).fail(function(err) {
				res.send({
					code: 500,
					message: 'FAIL_SYSTEM',
					data: err
				});
			});

		});

		//Authentication for FCONNECT endpoints
		app.get('/auth/facebook',
			passport.authenticate('facebook', {
				scope: ['email']
			}));

		app.get('/auth/facebook/callback',
			passport.authenticate('facebook', {
				failureRedirect: '/auth/facebook'
			}),
			function(req, res) {
				//get user info and IP value
				var name = req.user.displayName.replace(/" "/g, "**0**")
					.replace(/ç/g, "--1--")
					.replace(/Ç/g, "--2--")
					.replace(/ğ/g, "--3--")
					.replace(/Ğ/g, "--4--")
					.replace(/ı/g, "--5--")
					.replace(/İ/g, "--6--")
					.replace(/ö/g, "--7--")
					.replace(/Ö/g, "--8--")
					.replace(/ş/g, "--9--")
					.replace(/Ş/g, "--10--")
					.replace(/ü/g, "--11--")
					.replace(/Ü/g, "--12--");

				if (req.user.hasOwnProperty("emails") == true) {

					res.redirect('/campaign.html'+'?'+'name='+ name + '&fbID=' + req.user.emails[0].value);

				} else {

					res.redirect('/campaign.html'+'?'+'name='+ name + '&fbID=' + req.user.photos[0].value);
				}
			}
		);

		//landing page
		app.get('/campaign', function(req, res) {
			res.sendFile(path.join(__dirname, '../public', 'campaign.html'));
		});
		//interstitial page
		app.get('/interstitial', function(req, res) {
			res.sendFile(path.join(__dirname, '../public', 'interstitial.html'));
		});



		return app;
	}

	return {
		getInstance: function() {
			if (!instance)
				instance = init();

			return instance;

		}
	};
};

module.exports = exports = Api;