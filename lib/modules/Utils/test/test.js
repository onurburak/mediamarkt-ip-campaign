require('rootpath')();

var path = require('path'),
    Q = require('q'),
    crypto = require('crypto'),
    uuid = require('node-uuid'),
    fs = require('fs');


var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var should = chai.should();
var expect = chai.expect;

var pathTestConfig = path.join('modules', 'Utils', 'test', 'testConfig.json');

var config = require(path.join('lib', 'seamless-module', 'modules', 'Config')).getConfig();
config.load(pathTestConfig);

var mockEmptyObject = "";
var mockUndefinedObject = undefined;
var mockSomeObject = "Hello World";


describe('Utils',function(){
   describe('#exists_or_null', function(){
       it('should be a function', function(){
           var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
           return utils.exists_or_null.should.be.a('function');
       });
       it('should return null if no object is specified', function(){
           var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
           return expect(utils.exists_or_null(null)).to.be.null;
       });
       it('should return null if its empty after trimming', function(){
           var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
           return expect(utils.exists_or_null(mockEmptyObject)).to.be.null;
       });
       it('should return null if its undefined', function(){
           var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
           return expect(utils.exists_or_null(mockUndefinedObject)).to.be.null;
       });
       it('should return itself if it fulfills the conditions', function(){
           var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
           return expect(utils.exists_or_null(mockSomeObject)).to.equal(mockSomeObject);
       });
   });

    describe('#hash_password', function(){
        it('should be a function', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return utils.hash_password.should.be.a('function');
        });
        it('should return a hex value', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return expect(utils.hash_password(mockSomeObject)).to.be.hexadecimal;
        });
        it('the hex value should be md5', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return expect(utils.hash_password(mockSomeObject)).to.satisfy(
                function(string){
//                    32 characters for md5 and 40 for sha1
                    return string.search(/[a-fA-F0-9]{32}/) == 0;
                });
        });
    });

    describe('#generate_key', function(){
        it('should be a function', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return utils.generate_key.should.be.a('function');
        });
        it('should return a hex value', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return expect(utils.generate_key()).to.be.hexadecimal;
        });
        it('the hex value should be sha1', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return expect(utils.generate_key()).to.satisfy(
//                    32 characters for md5 and 40 for sha1
                function(string){
                    return string.search(/[a-fA-F0-9]{40}/) == 0;
                });
        });
    });

    describe('#save_file_upload', function(){

        it('should be a function', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return utils.save_file_upload.should.be.a('function');
        });
        it('should be rejected with an error if the file cannot be read', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
//            No file to read
            var file = {path: path.join(__dirname, ".")};
            return utils.save_file_upload(file,".").should.be.rejectedWith(Error);
        });
        it('should be rejected with an error if the file cannot be written', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            var file = {path: path.join(__dirname, "testConfig.json")};
//            Nowhere to write but can read a file
            return utils.save_file_upload(file, path.join(__dirname, ".")).should.be.rejectedWith(Error);
        });
    });

    describe('#sign_request', function(){
        it('should be a function', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return utils.sign_request.should.be.a('function');
        });
        it('should return a hex value', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return expect(utils.sign_request('token', 'key')).to.be.hexadecimal;
        });
        it('the hex value should be sha256', function(){
            var utils = require(path.join('modules', 'Utils'))(path, Q, crypto, uuid, fs).getInstance();
            return expect(utils.sign_request('token', 'key')).to.satisfy(
//                    64 characters for sha256
                function(string){
                    return string.search(/[a-fA-F0-9]{64}/) == 0;
                });
        });
    });
});
