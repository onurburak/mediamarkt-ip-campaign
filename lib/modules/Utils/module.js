var Utils = function (mongoSkin) {

  var instance;

  function init() {

    return {

      toObjectID: function(val){
        return mongoSkin.helper.toObjectID(val);
      },

    };
  }

  return {

    getInstance: function () {

      if (!instance) {
        instance = init();
      }

      return instance;
    }

  };

};


module.exports = exports = Utils;
