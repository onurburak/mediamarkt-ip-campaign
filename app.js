require('rootpath')();
var express = require("express"),
	http = require("http"),
	bodyParser = require('body-parser'),
	passport = require('passport'),
	FacebookStrategy = require('passport-facebook').Strategy,
	path = require('path'),
	mongoSkin = require('mongoskin'),
	Q = require('q'),
	async = require('async'),
	_ = require('underscore');

var mongoConfig = require(path.join('lib', 'modules', 'Config')).getConfig();
mongoConfig.load({
	mongoAddr: (/*'onurburak.com:27017'*/ 'localhost:27017' ),
	mongoDb: 'ip-test',
	mongoRs: (process.env.MONGO_RS || 'true')
});

var MongoDbFactory = require(path.join('lib', 'modules', 'MongoDbFactory'));
var mongoDbFactory = new MongoDbFactory();
var mongoDb = mongoDbFactory.getDb(mongoSkin, mongoConfig);
var StoreFactory = require(path.join('lib', 'modules', 'StoreFactory'));
var storeFactory = new StoreFactory();
var store = storeFactory.getStore(Q, async, _, mongoDb);

var utils = require(path.join('lib', 'modules' ,'Utils'))(mongoSkin).getInstance();


var app = require(path.join('api'))(path, express, bodyParser, passport, FacebookStrategy,store,utils).getInstance();


var server = http.createServer(app).listen(app.get('port'), function() {
	console.log('App listens port ' + server.address().port);


});
