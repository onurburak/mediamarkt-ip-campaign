//SCRIPT FOR LANDING PAGE

var gifts = [
	"ACER ES1 331 C0V4 13.3 inç Intel Celeron N3050 1.6 GHz 2 GB 32 GB Windows 8.1 Notebook",
	"APPLE iPhone 6s 16 GB Akıllı Telefon",
	"APPLE MD785TU/B iPad Air 16 GB 9.7 inç Wi-Fi ",
	"APPLE MJ3T2TU/A Watch 42 mm Uzay Grisi Alüminyum Kasa ve Siyah Spor Kordon",
	"APPLE MJVE2TU/A MacBook Air 13.3 inç Intel Core i-5 1.6 GHz 4 GB 128 GB Notebook",
	"BEATS BT.900.00157.03 Solo HD Control Talk OE Kulaküstü Kulaklık",
	"BRAUN 1 Serisi Traş Makinesi",
	"BRAUN 3170 Silk Epil Soft Perfection",
	"CANON EOS 100D 18-55 IS STM Lens Kit 18 MP SLR Fotoğraf Makinesi",
	"GO PRO HERO4 Black Adventure 5GPR/CHDHX-401-EU Aksiyon Kamera",
	"KINGSTON DTHX30 Data Traveler Hyperx 3.0 64GB USB Bellek",
	"LENOVO G50-45 15.6 inç AMD A8 6410 4 GB 500 GB Radeon R5 M330 2GB 80E301SCTX Notebook",
	"LG 43UF6807 43 inç 109 cm Ekran Ultra HD 4K SMART LED TV",
	"LG G4 Beat Titan Akıllı Telefon",
	"LG H815 G4 Gerçek Deri Akıllı Telefon",
	"MICROSOFT Xbox 360 500 GB Kinect + Kinect Sports Ultimate + Kinect Adventures (XBox 360 Bundle Set 33)",
	"NESPRESSO İnissia D45 Kahve Makinesi Aero 3 Süt Köpürtme Cihazı",
	"NIKON D3300 + 18-55 mm VR Lens Kit Dijital SLR Fotoğraf Makinesi",
	"PHILIPS Lumea IPL Prestige SC2007/00 Epilasyon Tüy Alma Sistemi",
	"SAMSUNG Galaxy Note 5 N920 32 GB Gold Akıllı Telefon",
	"SAMSUNG Galaxy Note 5 N920 32 GB Siyah Akıllı Telefon",
	"SAMSUNG Gear S2 Classic Akıllı Saat",
	"SAMSUNG SM T113NDWATUR Galaxy Tab 3 Wi-Fi 7 inç 1 GB 8 GB Android 4.4 Kit Kat Tablet PC ",
	"SONY PlayStation4 1TB Konsol",
	"SONY SWR30 SmartBand Talk Akıllı Bileklik Beyaz",
	"TEFAL GV6770 Effectis Easy 5,2 Bar 2200 W Buhar Kazanlı Ütü",
	"VESTEL 40FA5000 40 inç 102 cm Ekran Full HD LED TV Dahili Uydu Alıcılı"
];

$(document).ready(function() {


	var film_roll = new FilmRoll({
		container: '#slider',
		height: 200,
		scroll: false,
		configure_load: true,
		pager: false,
		easing: "linear",
		animation: 500
	});

	$('#slider').on('film_roll:moved', function(event) {
		window.dispatchEvent(new Event('resize'));
	});



	checker();
	//Select Product Button
	$("button.selectBtn").click(function() {
		$("div#scene-1,div#scene-2").toggleClass("hide");
	});
	//FCONNECT Button
	$("button.facebookBtn").click(function() {
		//ajax
		var cookie = uniqueCookie();
		createCookie("mediamarkt-cookie", cookie, 15);

		var selection = $(".active").data("film-roll-child-id");
		createCookie("mediamarkt-selection", selection, 15);

		top.location.assign("http://ip.mobilike.rocks/auth/facebook");
	});
});

function checker() {
	console.log(document.location.search);
	//check for query string in URL
	if (document.location.search.length) {
		var name = getParameterByName("name").replace(/--1--/g, "ç")
			.replace(/--2--/g, "Ç")
			.replace(/--3--/g, "ğ")
			.replace(/--4--/g, "Ğ")
			.replace(/--5--/g, "ı")
			.replace(/--6--/g, "İ")
			.replace(/--7--/g, "ö")
			.replace(/--8--/g, "Ö")
			.replace(/--9--/g, "ş")
			.replace(/--10--/g, "Ş")
			.replace(/--11--/g, "ü")
			.replace(/--12--/g, "Ü");

		var user = {
			name: name,
			email: getParameterByName("fbID"),
			selection: readCookie("mediamarkt-selection"),
			cookie: readCookie("mediamarkt-cookie")
		};

		$.ajax({
			url: 'http://ip.mobilike.rocks/save',
			type: 'POST',
			data: user,
			error: function(err) {
				console.log(err);
			},
			success: function(data) {
				console.log(data);
				$("div.scene").addClass("hide");
				$("div#scene-3").removeClass("hide");
			}
		});
	} else {
		var cookie = readCookie("mediamarkt-cookie");
		var url;
		console.log(cookie);
		if (cookie)
			url = 'http://ip.mobilike.rocks/check?cookie=' + cookie;
		else
			url = 'http://ip.mobilike.rocks/check';

		$.ajax({
			url: url,
			type: 'GET',
			error: function(err) {
				console.log(err);
			},
			success: function(obj) {
				console.log(obj);
				if (obj.hasOwnProperty("data") && !cookie) { //Bu IP'ye kayıtlı birileri varsa
					if (obj.data.length > 0) { //Bu IP'de bu kullanıcı dışında birileri varsa
						// var name = obj.data[0].name;
						// var selection = parseInt(obj.data[0].selection);
						// name = name.split(' ')[0];
						// $("#name").text(name);
						// $("div.gift img").attr("src", "images/gifts/" + (parseInt(obj.data[0].selection) + 1) + ".png");
						// $(".small").text(gifts[selection]);
						// //var selection = obj.data[0].selection;
						// $("div.scene").addClass("hide");
						// $("div#scene-4").removeClass("hide");
						console.log("Bu IP'de birileri var.");

						$("div.scene").addClass("hide");
						$("div#scene-1").removeClass("hide");

					} else { //Bu IP'de kayıtlı tek kişi şu anki kullanıcıysa
						console.log("BU IP'de ki tek kullanıcı sensin.");
						$("div.scene").addClass("hide");
						$("div#scene-3").removeClass("hide");
					}
				} else if(cookie && obj.hasOwnProperty("data") ){
					console.log("Bu IP'de birileri var ama sen hediyeni seçtin");

						$("div.scene").addClass("hide");
						$("div#scene-3").removeClass("hide");
				}else { //Bu IP'de kimse daha yoksa
					console.log("Bu IP'de kimse yok.");
				}
			}

		});
	}
}

function createCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else var expires = "";
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function uniqueCookie() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
}