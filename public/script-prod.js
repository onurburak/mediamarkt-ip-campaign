var gifts = [{
	name: "ACER ES1 331 C0V4 13.3 inç Intel Celeron N3050 1.6 GHz 2 GB 32 GB Windows 8.1 Notebook",
	URL: "http://www.mediamarkt.com.tr/catentry/1147451"
}, {
	name: "APPLE iPhone 6s 16 GB Akıllı Telefon",
	URL: "http://www.mediamarkt.com.tr/catentry/1148662"
}, {
	name: "APPLE MD785TU/B iPad Air 16 GB 9.7 inç Wi-Fi ",
	URL: "http://www.mediamarkt.com.tr/catentry/1130574"
}, {
	name: "APPLE MJ3T2TU/A Watch 42 mm Uzay Grisi Alüminyum Kasa ve Siyah Spor Kordon",
	URL: "http://www.mediamarkt.com.tr/tr/product/_apple-mj3t2tu-a-watch-42-mm-uzay-grisi-alüminyum-kasa-ve-siyah-spor-kordon-1147610.html"
}, {
	name: "APPLE MJVE2TU/A MacBook Air 13.3 inç Intel Core i-5 1.6 GHz 4 GB 128 GB Notebook",
	URL: "http://www.mediamarkt.com.tr/catentry/1137415"
}, {
	name: "BEATS BT.900.00157.03 Solo HD Control Talk OE Kulaküstü Kulaklık",
	URL: "http://www.mediamarkt.com.tr/tr/product/_beats-bt-900-00157-03-solo-hd-control-talk-oe-mor-kulaküstü-kulaklık-1115142.html"
}, {
	name: "BRAUN 1 Serisi Traş Makinesi",
	URL: "http://www.mediamarkt.com.tr/catentry/1072971"
}, {
	name: "BRAUN 3170 Silk Epil Soft Perfection",
	URL: "http://www.mediamarkt.com.tr/catentry/1024041"
}, {
	name: "CANON EOS 100D 18-55 IS STM Lens Kit 18 MP SLR Fotoğraf Makinesi",
	URL: "http://www.mediamarkt.com.tr/catentry/1099614"
}, {
	name: "GO PRO HERO4 Black Adventure 5GPR/CHDHX-401-EU Aksiyon Kamera",
	URL: "http://www.mediamarkt.com.tr/tr/product/_go-pro-hero4-black-adventure-5gpr-chdhx-401-eu-aksiyon-kamera-1128301.html"
}, {
	name: "KINGSTON DTHX30 Data Traveler Hyperx 3.0 64GB USB Bellek",
	URL: "http://www.mediamarkt.com.tr/tr/product/_kıngston-dthx30-data-traveler-hyperx-3-0-64-gb-usb-bellek-1130402.html"
}, {
	name: "LENOVO G50-45 15.6 inç AMD A8 6410 4 GB 500 GB Radeon R5 M330 2GB 80E301SCTX Notebook",
	URL: "http://www.mediamarkt.com.tr/catentry/1155178"
}, {
	name: "LG 43UF6807 43 inç 109 cm Ekran Ultra HD 4K SMART LED TV",
	URL: "http://www.mediamarkt.com.tr/catentry/1144141"
}, {
	name: "LG G4 Beat Titan Akıllı Telefon",
	URL: "http://www.mediamarkt.com.tr/catentry/1144634"
}, {
	name: "LG H815 G4 Gerçek Deri Akıllı Telefon",
	URL: "http://www.mediamarkt.com.tr/catentry/1139500"
}, {
	name: "MICROSOFT Xbox 360 500 GB Kinect + Kinect Sports Ultimate + Kinect Adventures (XBox 360 Bundle Set 33)",
	URL: "http://www.mediamarkt.com.tr/tr/product/_mıcrosoft-xbox-360-500-gb-kinect-kinect-sports-ultimate-kinect-adventures-xbox-360-bundle-set-33--1142134.html"
}, {
	name: "NESPRESSO İnissia D45 Kahve Makinesi Aero 3 Süt Köpürtme Cihazı",
	URL: "http://www.mediamarkt.com.tr/tr/product/_nespresso-inissia-d-45-kahve-makinesi-aero-3-süt-köpürtme-cihazı-siyah-1145297.html"
}, {
	name: "NIKON D3300 + 18-55 mm VR Lens Kit Dijital SLR Fotoğraf Makinesi",
	URL: "http://www.mediamarkt.com.tr/catentry/1116239"
}, {
	name: "PHILIPS Lumea IPL Prestige SC2007/00 Epilasyon Tüy Alma Sistemi",
	URL: "http://www.mediamarkt.com.tr/catentry/1145064"
}, {
	name: "SAMSUNG Galaxy Note 5 N920 32 GB Gold Akıllı Telefon",
	URL: "http://www.mediamarkt.com.tr/catentry/1145092"
}, {
	name: "SAMSUNG Galaxy Note 5 N920 32 GB Siyah Akıllı Telefon",
	URL: "http://www.mediamarkt.com.tr/catentry/1145093"
}, {
	name: "SAMSUNG Gear S2 Classic Akıllı Saat",
	URL: "http://www.mediamarkt.com.tr/catentry/1145093"
}, {
	name: "SAMSUNG SM T113NDWATUR Galaxy Tab 3 Wi-Fi 7 inç 1 GB 8 GB Android 4.4 Kit Kat Tablet PC ",
	URL: "http://www.mediamarkt.com.tr/catentry/1135694"
}, {
	name: "SONY PlayStation4 1TB Konsol",
	URL: "http://www.mediamarkt.com.tr/tr/product/_sony-playstation-4-1-tb-konsol-1144115.html"
}, {
	name: "SONY SWR30 SmartBand Talk Akıllı Bileklik Beyaz",
	URL: "http://www.mediamarkt.com.tr/tr/product/_sony-swr30-smartband-talk-akıllı-bileklik-beyaz-1129775.html"
}, {
	name: "TEFAL GV6770 Effectis Easy 5,2 Bar 2200 W Buhar Kazanlı Ütü",
	URL: "http://www.mediamarkt.com.tr/catentry/1129663"
}, {
	name: "VESTEL 40FA5000 40 inç 102 cm Ekran Full HD LED TV Dahili Uydu Alıcılı",
	URL: "http://www.mediamarkt.com.tr/catentry/1129361"
}];

var db;

$(document).ready(function() {
	if (window.openDatabase) {
		db = openDatabase('mydb', '1.0', 'my first database', 2 * 1024 * 1024);
		db.transaction(function(tx) {
			tx.executeSql('CREATE TABLE IF NOT EXISTS myDB (id unique, text)');
		});
	}

	var film_roll = new FilmRoll({
		container: '#slider',
		height: 200,
		scroll: false,
		configure_load: true,
		pager: false,
		easing: "linear",
		animation: 500,
	});

	$('#slider').on('film_roll:moved', function(event) {
		window.dispatchEvent(new Event('resize'));
	});


	$('#slider').swipe({
		//Generic swipe handler for all directions
		swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
			console.log(direction);
		},
		//Default is 75px, set to 0 for demo so any distance triggers swipe
		threshold: 0
	});



	checker();
	//Select Product Button
	$("button.selectBtn").click(function() {
		$("div#scene-1,div#scene-2").toggleClass("hide");
	});
	//FCONNECT Button
	$("button.facebookBtn").click(function() {
		//ajax
		if (getUrlVars()["cookie"])
			var cookie = getUrlVars()["cookie"]
		else
			var cookie = uniqueCookie();
		console.log(cookie);

		var selection = $(".active").data("film-roll-child-id");
		if (!window.openDatabase) {
			eraseCookie("mediamarkt-cookie");
			eraseCookie("mediamarkt-selection");

			createCookie("mediamarkt-cookie", cookie, 15);
			createCookie("mediamarkt-selection", selection, 15);

			window.location.assign("http://ip.mobilike.rocks/auth/facebook");
		} else {
			db.transaction(function(tx) {
				tx.executeSql("DELETE FROM myDB WHERE id=5");
				tx.executeSql("DELETE FROM myDB WHERE id=10");

				tx.executeSql('INSERT INTO myDB (id, text) VALUES (5, "' + cookie + '")');
				tx.executeSql('INSERT INTO myDB (id, text) VALUES (10, "' + selection + '")');
				console.log(selection);

				window.location.assign("http://ip.mobilike.rocks/auth/facebook");
			});
		}


	});
});

function checker() {
	var result = [];
	if (!window.openDatabase) {
		if (getUrlVars()["name"] != undefined) {
			var name = getParameterByName("name").replace(/--1--/g, "ç")
				.replace(/--2--/g, "Ç")
				.replace(/--3--/g, "ğ")
				.replace(/--4--/g, "Ğ")
				.replace(/--5--/g, "ı")
				.replace(/--6--/g, "İ")
				.replace(/--7--/g, "ö")
				.replace(/--8--/g, "Ö")
				.replace(/--9--/g, "ş")
				.replace(/--10--/g, "Ş")
				.replace(/--11--/g, "ü")
				.replace(/--12--/g, "Ü");

			var user = {
				name: name,
				email: getParameterByName("fbID"),
				selection: readCookie("mediamarkt-selection"),
				cookie: readCookie("mediamarkt-cookie")
			};

			$.ajax({
				url: 'http://ip.mobilike.rocks/save',
				type: 'POST',
				data: user,
				error: function(err) {
					console.log(err);
				},
				success: function(data) {
					console.log(data);
					$("div.scene").addClass("hide");
					$("div#scene-3").removeClass("hide");
				}
			});
		} else {
			var cookie = readCookie("mediamarkt-cookie");
			var url;
			console.log(cookie);
			if (cookie)
				url = 'http://ip.mobilike.rocks/check?cookie=' + cookie;
			else
				url = 'http://ip.mobilike.rocks/check';

			$.ajax({
				url: url,
				type: 'GET',
				error: function(err) {
					console.log(err);
				},
				success: function(obj) {
					console.log(obj);
					if (obj.hasOwnProperty("data")) { //Bu IP'ye kayıtlı birileri varsa
						if (obj.data.length > 0) { //Bu IP'de bu kullanıcı dışında birileri varsa

							console.log("Bu IP'de birileri var.");
							var name = obj.data[0].name;
							var selection = parseInt(obj.data[0].selection);
							name = name.split(' ')[0];
							$("#name").text(name);
							$("div.gift img").attr("src", "http://ip.mobilike.rocks/images/gifts/" + (parseInt(obj.data[0].selection) + 1) + ".png");
							$(".small").text(gifts[selection].name);
							//Buy Now Button
							$("button.buyNowBtn").click(function() {
								window.location.assign(gifts[selection].URL);
							});
							$("div.scene").addClass("hide");
							$("div#scene-4").removeClass("hide");


						} else { //Bu IP'de kayıtlı tek kişi şu anki kullanıcıysa
							console.log("BU IP'de ki tek kullanıcı sensin.");
							$("img#thanks").attr("src", "http://ip.mobilike.rocks/images/youSelectedImg.png");
							$("div.scene").addClass("hide");
							$("div#scene-3").removeClass("hide");
						}
					} else { //Bu IP'de kimse daha yoksa
						console.log("Bu IP'de kimse yok.");
					}
				}

			});
		}
	} else {
		console.log(db);
		db.transaction(function(tx) {
			tx.executeSql('SELECT * FROM myDB', [], function(tx, results) {
				console.log(results);
				var len = results.rows.length,
					i;
				for (i = 0; i < len; i++) {
					console.log(results.rows.item(i).text);
					var x = results.rows.item(i).text.toString();
					result.push(x);
				}
				console.log(result);
				//check for query string in URL
				if (getUrlVars()["name"] != undefined) {
					var name = getParameterByName("name").replace(/--1--/g, "ç")
						.replace(/--2--/g, "Ç")
						.replace(/--3--/g, "ğ")
						.replace(/--4--/g, "Ğ")
						.replace(/--5--/g, "ı")
						.replace(/--6--/g, "İ")
						.replace(/--7--/g, "ö")
						.replace(/--8--/g, "Ö")
						.replace(/--9--/g, "ş")
						.replace(/--10--/g, "Ş")
						.replace(/--11--/g, "ü")
						.replace(/--12--/g, "Ü");

					var user = {
						name: name,
						email: getParameterByName("fbID"),
						selection: result[1],
						cookie: result[0]
					};

					$.ajax({
						url: 'http://ip.mobilike.rocks/save',
						type: 'POST',
						data: user,
						error: function(err) {
							console.log(err);
						},
						success: function(data) {
							console.log(data);
							$("div.scene").addClass("hide");
							$("div#scene-3").removeClass("hide");
						}
					});
				} else {
					console.log(result);
					var cookie = result[0];
					var url;
					console.log(cookie);
					if (cookie)
						url = 'http://ip.mobilike.rocks/check?cookie=' + cookie;
					else
						url = 'http://ip.mobilike.rocks/check';

					$.ajax({
						url: url,
						type: 'GET',
						error: function(err) {
							console.log(err);
						},
						success: function(obj) {
							console.log(obj);
							if (obj.hasOwnProperty("data")) { //Bu IP'ye kayıtlı birileri varsa
								if (obj.data.length > 0) { //Bu IP'de bu kullanıcı dışında birileri varsa

									console.log("Bu IP'de birileri var ve bu sen değilsin.");
									// var name = obj.data[0].name;
									// var selection = parseInt(obj.data[0].selection);
									// name = name.split(' ')[0];
									// $("#name").text(name);
									// $("div.gift img").attr("src", "http://ip.mobilike.rocks/images/gifts/" + (parseInt(obj.data[0].selection) + 1) + ".png");
									// $(".small").text(gifts[selection].name);
									// //Buy Now Button
									// $("button.buyNowBtn").click(function() {
									// 	window.location.assign(gifts[selection].URL);
									// });
									// $("div.scene").addClass("hide");
									// $("div#scene-4").removeClass("hide");


								} else { //Bu IP'de kayıtlı tek kişi şu anki kullanıcıysa
									console.log("BU IP'de ki tek kullanıcı sensin.");
									$("img#thanks").attr("src", "http://ip.mobilike.rocks/images/youSelectedImg.png");
									$("div.scene").addClass("hide");
									$("div#scene-3").removeClass("hide");
								}
							} else { //Bu IP'de kimse daha yoksa
								console.log("Bu IP'de kimse yok.");
							}
						}

					});
				}

			});
		});
	}


}

function createCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else var expires = "";
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function uniqueCookie() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
}

function getUrlVars() {
	var vars = [],
		hash;
	var hashes = window.parent.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	for (var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}